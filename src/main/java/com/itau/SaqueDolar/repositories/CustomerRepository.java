package com.itau.SaqueDolar.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.SaqueDolar.models.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{

	public Optional<Customer> findByUserName(String userName);
}
