package com.itau.SaqueDolar.models;

import java.util.HashMap;

public class RatesResponse {

	private String Base;
	
	private int timestamp;
	
	private String date;
	
	private HashMap<String, String> rates;

	public String getBase() {
		return Base;
	}

	public void setBase(String base) {
		Base = base;
	}

	public int getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public HashMap<String, String> getRates() {
		return rates;
	}

	public void setRates(HashMap<String, String> rates) {
		this.rates = rates;
	}
	
	
}
