package com.itau.SaqueDolar.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.SaqueDolar.models.RatesResponse;

@Service
public class RatesConverter {
	
	private RatesResponse ratesResponse;
	
	RestTemplate restTemplate = new RestTemplate();

	public double Convert(String to, double amount) {
		double valorConvertido = 0;
		RatesResponse ratesResponse = getRates(to);
		
		valorConvertido = Double.valueOf(ratesResponse.getRates().get(to));
		valorConvertido = amount * valorConvertido;
		return valorConvertido;
	}
	
	public RatesResponse getRates(String to) {
		String url = "http://data.fixer.io/api/latest?access_key=19d13a140834c1241593ec64a895403d&base=EUR&symbols=" + to;
		
		this.ratesResponse = restTemplate.getForObject(url, RatesResponse.class);
		
		return this.ratesResponse;
	}	
}
