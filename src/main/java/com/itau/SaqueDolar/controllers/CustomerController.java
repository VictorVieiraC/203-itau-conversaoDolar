package com.itau.SaqueDolar.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.SaqueDolar.models.Customer;
import com.itau.SaqueDolar.models.WithdrawRequest;
import com.itau.SaqueDolar.repositories.CustomerRepository;
import com.itau.SaqueDolar.services.RatesConverter;

@RestController
public class CustomerController {

	@Autowired
	RatesConverter ratesConverter;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@RequestMapping(path="/converter/{amount}/{to}")
	public double ChamarConvert(@PathVariable String to, @PathVariable double amount) {
		
		double valorConvertido = ratesConverter.Convert(to, amount);
		
		return valorConvertido;
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/converter")
	public ResponseEntity<?> Withdraw(@Valid @RequestBody WithdrawRequest request){
		Optional<Customer> optionalCustomer = customerRepository.findByUserName(request.getUserName());
		double saque = ChamarConvert(request.getCurrency(), request.getAmount());
		
		if (!optionalCustomer.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		else if (Double.valueOf(optionalCustomer.get().getBalance()) >= saque) {
			optionalCustomer.get().setBalance(String.valueOf(Double.valueOf(optionalCustomer.get().getBalance()) - saque));
			customerRepository.save(optionalCustomer.get());
			return ResponseEntity.ok().body(optionalCustomer.get());
		}
		else {
			return ResponseEntity.status(400).build();
		}

	}

}
